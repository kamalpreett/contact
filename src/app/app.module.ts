import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { SaveComponent } from './save/save.component';
import { FormComponent } from './form/form.component';
import { DataCommunicationService } from './data-communication.service';
const routes: Routes = [{
  path:"",
  component:FormComponent
}, {
  path: "save",
  component:SaveComponent
}]

@NgModule({
  declarations: [
    AppComponent,
    SaveComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [DataCommunicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
