import { Component, OnInit } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { DataCommunicationService } from '../data-communication.service';

@Component({
  selector: 'app-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.css']
})

export class SaveComponent implements OnInit {

firstName:string;
lastName:string;
email:string;
password:string;
cpassword:string;
mobile:string;
empId:string;
gender:string;
formData;

  constructor(private router:Router, private data:DataCommunicationService) {
    this.formData=data.getData();
    // this.formData=JSON.parse(localStorage.getItem("formData"));
    this.firstName=this.formData.firstName;
    this.lastName=this.formData.lastName;
    this.email=this.formData.email;
    this.password=this.formData.password;
    this.cpassword=this.formData.cpassword;
    this.mobile=this.formData.mobile;
    this.empId=this.formData.empId;
    this.gender=this.formData.gender;
   }

  ngOnInit() {
  }

  edit(){
    this.router.navigate(['']);
  }
  
}
