import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataCommunicationService {
  formData
  constructor() { }
  setData(data) {
    this.formData = data;
  }

  getData() {
    return this.formData;
  }
}
