import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataCommunicationService } from '../data-communication.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  constructor(private router: Router, private data: DataCommunicationService) {

  }

  firstName: string;
  lastName: string;
  email: string;
  password: string;
  mobile: string;
  gender: string;
  empId: string;
  cpassword: string;
  formData;
  isEqual: boolean = true;
  type: string = "password";

  ngOnInit() {
    if (this.data.getData() != null) {
      this.formData = this.data.getData();
      // this.formData = JSON.parse(localStorage.getItem("formData"));
      this.signup_form.setValue({
        firstName: this.formData.firstName,
        lastName: this.formData.lastName,
        email: this.formData.email,
        password: this.formData.password,
        mobile: this.formData.mobile,
        gender: this.formData.gender,
        empId: this.formData.empId,
        cpassword: this.formData.cpassword
      })
    }
  }

  signup_form = new FormGroup({
    firstName: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z]+")]),
    lastName: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z]+")]),
    email: new FormControl('', [Validators.required, Validators.email]),
    mobile: new FormControl('', [Validators.required, Validators.pattern("[0-9]{10}"), Validators.maxLength(10)]),
    password: new FormControl('', [Validators.required, Validators.pattern("^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$")]),
    cpassword: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z]+")]),
    empId: new FormControl('', [Validators.required, Validators.pattern("[0-9]{4}")])
  });

  comparePassword(confirmPassword) {
    if (confirmPassword.target.value === this.signup_form.value.password) {
      this.isEqual = true;
    }
    else {
      this.isEqual = false;
    }
  }
  show() {
    this.type = "text";
  }
  hide() {
    this.type = "password";
  }
  save() {
    this.data.setData(this.signup_form.value)
    // localStorage.setItem("formData", JSON.stringify(this.signup_form.value));
    this.router.navigate(['/save']);
  }

}
